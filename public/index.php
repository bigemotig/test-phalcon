<?php

use Phalcon\Config\Adapter\Yaml;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;

date_default_timezone_set('Europe/Moscow');
header('Content-Type: text/html; charset=utf-8');

define('SRC_PATH', dirname(__DIR__));
define('APP_PATH', dirname(__DIR__).'/app');

try {
    require SRC_PATH.'/vendor/autoload.php';
    $config = new Yaml(APP_PATH.'/config/config.yaml');
    $di = new FactoryDefault();

    require APP_PATH.'/config/services.php';

    (new Application($di))->handle()->send();
} catch(Exception|Error $e) {
    die($e->getMessage());
}

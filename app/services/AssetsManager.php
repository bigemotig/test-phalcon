<?php namespace app\services;

use Phalcon\Assets\Filters\Cssmin;
use Phalcon\Assets\Filters\Jsmin;
use Phalcon\Mvc\User\Component;

class AssetsManager extends Component
{
    public function register() {
        $this->assets
            ->collection('style')
            ->addCss('css/font-awesome.min.css')
            ->addCss('css/materialize.min.css')
            ->addCss('css/materialize-theme.css')
            ->addCss('css/style.css')
            ->setTargetPath('css/final.css')
            ->join(true)
            ->setTargetUri('css/final.css')
            ->addFilter(new Cssmin());

        $this->assets
            ->collection('final')
            ->addJs('js/jquery-2.1.4.min.js')
            ->addJs('js/common.js')
            ->addJs('js/materialize.min.js')
            ->addJs('js/mobile_application.js')
            ->setTargetPath('js/final.js')
            ->join(true)
            ->setTargetUri('js/final.js')
            ->addFilter(new Jsmin());
    }
}

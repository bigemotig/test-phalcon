<?php namespace app\plugins;

use Phalcon\Dispatcher;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;

class NotFound404 extends Plugin {

    public function beforeDispatch(Event $event, Dispatcher $dispatcher) {
        $className = $dispatcher->getHandlerClass();
        $actionName = $dispatcher->getActionName();

        if (!method_exists($className, $actionName.'Action')) {
            return $dispatcher->forward([
                'controller' => 'error',
                'action' => 'show404'
            ]);
        }
    }
}

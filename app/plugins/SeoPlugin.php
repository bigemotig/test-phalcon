<?php namespace app\plugins;

use app\models\Controller;
use app\models\SeoPage;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;

class SeoPlugin extends Plugin {
    const DEFAULT = [
        'title' => '',
        'keywords' => '',
        'description' => ''
    ];

    protected function getSeo() {
        /** @var Controller $controller */
        $controller =  Controller::findFirst([
            'page_url = ?0 AND action_url = ?1',
            'bind' => [
                $this->router->getControllerName(),
                $this->router->getActionName(),
            ]
        ]);
        if (!$controller)
            return SeoPlugin::DEFAULT;

        $id = $this->request->getQuery('id') ?? null;
        $conditions = 'controller_id = ?0';
        $conditions .= $id ? ' and post_id = '.$id : ' and post_id IS NULL';
        $seo = SeoPage::findFirst([
            $conditions,
            'bind' => [$controller->getId()]
        ]);
        return $seo ? $seo->toArray() : SeoPlugin::DEFAULT;
    }

    public function beforeDispatch(Event $event, Dispatcher $dispatcher) {
        $this->view->setVar('seo', $this->getSeo());
    }
}





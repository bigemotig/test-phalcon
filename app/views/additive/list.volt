<div class="section" id="additive_list">
    <div class="row">
        <div class="col s12 offset-m2 m8">
            <ul class="collapsible popout" data-collapsible="accordion">
                {% for collapsible in additive_list %}
                    <li>
                        <div class="collapsible-header" id="additives">{{ collapsible['spec'] }} (E{{ collapsible['min'] }} — E{{ collapsible['max'] }})</div>
                        <div class="collapsible-body">
                            {% if collapsible['additives'] %}
                                {% for additive in collapsible['additives'] %}
                                    <a href="/additive/show?id={{ additive['id'] }}" title="{{ additive['title'] }}">
                                        <div class="row additive">
                                            <div class="col s1 m1">{{ additive['code'] }}</div>
                                            <div class="col s10 m10" style="margin-left: 20px;">{{ additive['spec'] }}</div>
                                        </div>
                                    </a>
                                {% endfor %}
                            {% endif %}
                        </div>
                    </li>
                {% endfor %}
            </ul>
        </div>
    </div>
</div>

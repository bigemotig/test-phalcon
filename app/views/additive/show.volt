<div class="section" id="additive_show">
    <div class="row">
        <div class="col s12 m12 offset-l2 l8">
            <h2 class="start-feature-heading">Название</h2>
            {% if additive['spec']|length > 1 %}
                <ul class="grey-text text-darken-1 start-paragraph-text">
                    {% for li in additive['spec'] %}
                       <li>{{ li }}</li>
                    {% endfor %}
                </ul>
            {% else %}
                <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['spec'][0] }}</p>
            {% endif %}

            <h2 class="start text-primarycolor">Технологические функции</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['functions'] }}</p>

            <h2 class="start text-primarycolor">Внешний вид</h2>
            {% if additive['appearance']|length > 0 %}
                <ul class="grey-text text-darken-1 start-paragraph-text">
                    {% for li in  additive['appearance'] %}
                        <li>{{ li }}</li>
                    {% endfor %}
                </ul>
            {% else %}
                <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['appearance'] }}</p>
            {% endif %}

            {% if additive['negative_effects'] is defined %}
                <h2 class="start text-primarycolor">Токсичность</h2>
                <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['negative_effects'] }}</p>
            {% endif %}

            <h2 class="start text-primarycolor">Природный источник</h2>
            {% if additive['extracted_from']|length > 1 %}
                <ul class="grey-text text-darken-1 start-paragraph-text">
                    {% for li in additive['extracted_from'] %}
                        <li>{{ li }}</li>
                    {% endfor %}
                </ul>
            {% else %}
                <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['extracted_from'][0] }}</p>
            {% endif %}

            <h2 class="start text-primarycolor">Евразийский экономический союз</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['in_russia'] }}</p>

            <h2 class="start text-primarycolor">Европейский союз</h2>
            <p class="grey-text text-darken-1 start-paragraph-text">{{ additive['in_eu'] }}</p>

            <h2 class="start text-primarycolor">Применение</h2>
            {% if additive['employment'] is defined %}
                {% for li in additive['employment'] %}
                    <p class="grey-text text-darken-1 start-paragraph-text">{{ li }}</p>
                {% endfor %}
            {% endif %}
        </div>

        <div class="col s12 m12 offset-l2 l8">
            {% if additive['prev_id'] is defined %}
                <a class="waves-effect keyboard arrow left btn" href="/additive/show?id={{ additive['prev_id'] }}" title="{{ additive['prev_title'] }}">
                    <i class="fa fa-chevron-left fa-1"></i><span> {{ additive['prev_code'] }}</span>
                </a>
            {% endif %}

            {% if additive['next_id'] is defined %}
                <a class="waves-effect waves-light btn right" href="/additive/show?id={{ additive['next_id'] }}" title="{{ additive['next_title'] }}">
                    <span>{{ additive['next_code'] }}</span><i class="fa fa-chevron-right"></i>
                </a>
            {% endif %}
        </div>
    </div>
</div>

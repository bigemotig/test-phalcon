<div class="section" id="mobile_app">
    <div class="row">
        <div class="col s12 m12" style="text-align: center">
            <h2 class="title">Бесплатный справочник</h2>
        </div>

        <div class="col s12  offset-m2 m8 l6">
            <img class="responsive-img" src="/img/prilozhenie-edobavki.png" alt="ЕДобавки android приложение">
        </div>

        <div class="col s12 m12 l6">
            <h3 class="center">О программе</h3>

            <p>
                Предлагаем Вам попробовать новое бесплатное мобильное приложение <b>{{ site_name }}</b>, разработанное под
                операционную систему Android. От аналогов его отличает простой и интуитивно понятный интерфейс, полная и
                актуальная база пищевых добавок (на данный момент 498 шт.), а также удобное управление, основанное на
                рекомендациях создателей операционной системы. Поскольку приложение работает без доступа в Интернет, Вы
                всегда сможете узнать всю необходимую информацию.
            </p>

            <a class="waves-effect waves-light btn col s12  offset-m2 m8 offset-l3 l6" id="download" title="Установить приложение ЕДобавки" href="https://play.google.com/store/apps/details?id=top.edobavki">Установить</a>
        </div>
    </div>

    <div class="row center" id="screenshots">
        <div class="col s12 m12">
            <h2 class="title center">Скриншоты</h2>
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="/img/edobavki-spisok.jpg" alt="Список пищевых добавок с их кодами">
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="/img/edobavki-kartochka.jpg" alt="Карточка пищевой добавки">
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="/img/edobavki-menyu.jpg" alt="Левое выдвигающиеся меню приложения">
        </div>
        <div class="col s12 m6 l3">
            <img class="responsive-img" src="/img/edobavki-nastrojki.jpg" alt="Настроки мобильного справочника ЕДобавки">
        </div>
    </div>

    <div class="row" id="cards_block">
        <div class="col s12 m12">
            <h2 class="title center">Почему именно наше приложение?</h2>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-smile-o fa-2x"></i>
                    <span class="card-title">Дизайн</span>
                    <p class="bounceEffect">
                        Простой и интуитивно понятный интерфейс.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-rocket fa-2x"></i>
                    <span class="card-title">Удобство</span>
                    <p class="bounceEffect">
                        Быстрый поиск и удобная навигация.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-signal fa-2x"></i>
                    <span class="card-title">Сеть</span>
                    <p class="bounceEffect">
                        Функционирует без доступа в Интернет.
                    </p>
                </div>
            </div>
        </div>
        <div class="col s12 m6 l3">
            <div class="card blue">
                <div class="card-content white-text">
                    <i class="fa fa-pie-chart fa-2x"></i>
                    <span class="card-title">Поддержка</span>
                    <p class="bounceEffect">
                        Работает на 99 процентах Android устройств.
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m12" style="height: 5em">
            <a class="waves-effect waves-light btn col s12  offset-m2 m8 offset-l4 l4" id="download" title="Установить приложение ЕДобавки" href="https://play.google.com/store/apps/details?id=top.edobavki">Установить</a>
        </div>
    </div>
</div>

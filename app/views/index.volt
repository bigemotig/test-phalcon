<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="ru-RU">

<head>
    <title>{{ seo['title'] }}</title>
    {{ assets.outputCss('style') }}
    <link rel="icon" sizes="16x16" href="{{ url('img/favicon.ico') }}">
    <meta charset="UTF-8" />
    <meta name="keywords" content="{{ seo['keywords'] }}">
    <meta name="description" content="{{ seo['description'] }}">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    {% if environment == 'prod' %}
        <!-- Google START -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-93153629-1', 'auto');
            ga('send', 'pageview');

        </script>
        <!-- Google END -->

        <!-- Yandex START -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter43276364 = new Ya.Metrika({
                            id:43276364,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/43276364" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- Yandex END -->
    {% endif %}
</head>

<body>

    <header>
        <nav>
            <div class="nav-wrapper container">
                <a href="#" data-activates="nav-mobile" class="button-collapse hide-on-large-only" id="hamburger"><i class="fa fa-bars"></i></a>
                <a class="brand-logo" href="/" title="Пищевые добавки и их влияние на организм человека"> {{ site_name }}</a>
                <ul id="nav-pc" class="right hide-on-med-and-down">
                    <li><a href="/" title="Пищевые добавки и их влияние на организм человека">Главная</a></li>
                    <li><a href="/additive/list" title="Пищевые добавки">Пищевые добавки</a></li>
                    <li><a href="/article/list" title="Список статей">Статьи</a></li>
                    <li><a href="/mobile_application" title="Мобильное приложение">Мобильное приложение</a></li>
                </ul>
                <ul class="right">
                    <li id="search-icon"><a title="Поиск"><i class="fa fa-search"></i></a></li>
                </ul>
                <form id="hide-form">
                    <div class="input-field" id="search-div" style="display: none;">
                        <input type="text" id="autocomplete" placeholder="Поиск по добавкам" autocomplete="off">
                        <label for="autocomplete" class="active"></label>
                        <i class="fa fa-times" id="close-icon"></i>
                        <ul class="autocomplete-content dropdown-content" id="search-results"></ul>
                    </div>
                </form>
                <ul id="nav-mobile" class="side-nav" style="transform: translateX(-100%);"><li>
                        <a href="/"><i class="fa fa-home"></i>Главная</a>
                    </li><li>
                        <a href="/additive/list"><i class="fa fa-th-list"></i>Пищевые добавки</a>
                    </li><li>
                        <a href="/article/list"><i class="fa fa-newspaper-o"></i>Статьи</a>
                    </li><li>
                        <a href="/mobile_application"><i class="fa fa-mobile"></i>Мобильное приложение</a>
                    </li></ul>
            </div>
        </nav>
    </header>

    {% if breadcrumb %}
        <div id="index-banner" class="parallax-container">
            <div class="section no-pad-bot">
                <div class="container">
                    <h1>{{ breadcrumb_title }}</h1>
                    <nav id="breadcrumbs">
                        <div class="nav-wrapper">
                            <div class="col s12" style="text-align: center;">
                                {% if is_main is defined %}
                                    <a href="/" class="breadcrumb" title="Пищевые добавки и их влияние на организм человека">Главная</a>
                                {% endif %}

                                {% for br in breadcrumbs %}
                                    <a {% if br['href'] is defined %} href="{{ br['href'] }}" title="{{ br['title'] }}" {% endif %} class="breadcrumb">{{ br['spec'] }}</a>
                                {% endfor %}
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="parallax">
                <img src="/img/breadcrumbs_background.jpg" style="display: block; transform: translate3d(-50%, 342px, 0px);" alt="фон хлебных крошек">
            </div>
        </div>
    {% endif %}

    <div id="wrapper" class="container">
        {{ content() }}
    </div>

    <footer class="page-footer center white-text teal">
        <div class="container">
            <div class="row">
                <a class="white-text" href="/about_us" title="Информация о проекте ЕДобавки">О сайте</a>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                2016 - {{ current_year }} ©. Копирование информации разрешено при условии рамещения ссылки.
            </div>
        </div>
    </footer>

    {{ assets.outputJs('final') }}
</body>
</html>

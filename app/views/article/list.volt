<div class="section col s6 m6" id="article_list">

    {% for article in article_list %}
        <div class="row">
            <div class="col s12 m7 offset-m1 offset-l3 l6">
                <div class="card large">
                    <div class="card-image">
                        <img src="/img/article/thumbs/{{ article['thumb_name'] }}">
                        <span class="card-title">{{ article['spec'] }}</span>
                    </div>
                    <div class="card-content">
                        <p>{{ article['announcement'] }}</p>
                    </div>
                    <div class="card-action">
                        <a href="/article/show?id={{ article['id'] }}">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
    {% endfor %}

</div>

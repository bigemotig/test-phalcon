<div class="section" id="start_page">
    <div class="row">
        <div class="col s12 offset-m2 m8 l6 center-align" id="app_block">
            <h2 class="center" style="margin-bottom: 2rem">Приложение для смартфонов</h2>
            <div>
                <img class="responsive-img" src="img/edobavki-preview.png" alt="Поиск в Android приложении ЕДобавки">
            </div>
            <a class="waves-effect waves-light btn col s12 offset-m2 m8 offset-l4 l4" href="/mobile_application" id="about_app" title="Мобильное приложение">Подробнее</a>
        </div>

        <div class="col s12 offset-m1 m10 l6" id="news_block">
            <div class="row">
                <h2 class="title center">Почитать</h2>
            </div>
            {% for article in articles %}
                <div class="col s12 m12">
                    <div class="row">
                        <a  href="/article/show?id={{ article['id'] }}"><h3 class="left">{{ article['spec'] }}</h3></a>
                    </div>
                    <div class="row">
                        <p class="light">
                            {{ article['announcement'] }}
                        </p>
                    </div>
                </div>
            {% endfor  %}
        </div>
    </div>
</div>

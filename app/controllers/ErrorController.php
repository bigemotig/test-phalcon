<?php namespace app\controllers;

class ErrorController extends BaseController
{
    public function show404Action()
    {
        $this->response->setStatusCode(404, "Not Found");
        $this->view->setVar('seo', [
            'title' => '404 Not Found',
            'keywords' => '',
            'description' => ''
        ]);
    }
}


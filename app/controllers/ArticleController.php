<?php namespace app\controllers;

use app\models\Article;

class ArticleController extends BaseController
{
    public function initialize()
    {
        $this->breadcrumb = true;
        parent::initialize();
    }

    public function listAction()
    {
        $model = new Article();
        $this->view->setVar('breadcrumb_title', 'Статьи');
        $this->view->setVar('article_list', $model->getArticles());
    }

    public function showAction()
    {
        $id = $this->request->getQuery('id');
        $article = Article::findFirst([
            'id = ?0',
            'bind' => [$id]
        ]);

        if (!$article) {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'show404',
            ]);
        }

        $article = $article->toArray();
        $this->breadcrumbs = [
            ['spec' => 'Статьи', 'href' => '/article/list', 'title' => 'Статьи'],
        ];
        $this->view->setVar('breadcrumb_title', $article['spec']);
        $this->view->setVar('breadcrumbs', $this->breadcrumbs);
        $this->assets->addCss('css/article_show.css');
        $this->view->setVar('article', $article);
    }
}


<?php namespace app\controllers;

use app\models\Additive;

class AdditiveController extends BaseController
{
    public function initialize()
    {
        $this->breadcrumb = true;
        parent::initialize();
    }

    public function listAction()
    {
        $this->view->setVar('breadcrumb_title', 'Пищевые добавки');
        $this->view->setVar('additive_list', $this->additiveList());
    }

    public function showAction() {
        $id = $this->request->getQuery('id');
        $additive = Additive::findFirst([
            'id = ?0',
            'bind' => [$id]
        ]);

        if (!$additive) {
            return $this->dispatcher->forward([
                'controller' => 'error',
                'action'     => 'show404',
            ]);
        }

        $adt = new Additive();
        $this->view->setVar('additive', $adt->fullInfo($id));
        $this->view->setVar('breadcrumb_title', $additive->getCode() .' пищевая добавка');
        $this->breadcrumbs = [
            ['spec' => 'Пищевые добавки', 'href' => '/additive/list', 'title' => 'Пищевые добавки'],
        ];
        $this->assets->addCss('css/additive_show.css');
        $this->view->setVar('additive_list', $this->additiveList());
        $this->view->setVar('breadcrumbs', $this->breadcrumbs);
    }

    public function collapseList() {
        $list = [
            ['spec' => 'Красители', 'min' => 100, 'max' => 199],
            ['spec' => 'Консерванты', 'min' => 200, 'max' => 299],
            ['spec' => 'Антиокислители', 'min' => 300, 'max' => 399],
            ['spec' => 'Стабилизаторы, загустители, эмульгаторы', 'min' => 400, 'max' => 499],
            ['spec' => 'Регуляторы кислотности и вещества против слёживания', 'min' => 500, 'max' => 599],
            ['spec' => 'Усилители вкуса и аромата, ароматизаторы', 'min' => 600, 'max' => 699],
//            ['spec' => 'Антибиотики', 'min' => 700, 'max' => 799],
            ['spec' => 'Прочие ', 'min' => 900, 'max' => 999],
            ['spec' => 'Дополнительные вещества ', 'min' => 1000, 'max' => 1999]
        ];
        return $list;
    }

    public function additiveList() {
        $collapse_list = $this->collapseList();
        $model = new Additive();
        $additives = $model->allAdditives()->toArray();

        foreach ($collapse_list as &$value) {
            foreach ($additives as $add) {
                if ($add['seq'] >= $value['min'] && $add['seq'] <= $value['max'])
                    $value['additives'][] = $add;
            }
        }
        return $collapse_list;
    }

    public function breadcrumbs() {
        $breadcrumbs = [];
        return $breadcrumbs;
    }
}


<?php namespace app\controllers;

use app\models\Additive;
use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

class BaseController extends Controller
{
    public $isMain              = false;
    public $breadcrumb          = false;
    public $breadcrumbs         = [];
    public $breadcrumbTitle     = '';

    public function initialize() {
        $this->assetsManager->register();

        $this->view->setVar('current_year', date('Y'));
        $this->view->setVar('site_name', $this->config->site->name);
        $this->view->setVar('environment', $this->config->environment);
        $this->view->setVar('breadcrumb_title', $this->breadcrumbTitle);
        $this->view->setVar('is_main', $this->isMain);
        $this->view->setVar('breadcrumb', $this->breadcrumb);
        $this->view->setVar('breadcrumbs', $this->breadcrumbs);
    }


    public function searchAction()
    {
        $this->view->disable();
        $search = $this->request->get('search');

        $fact = new Additive();
        $additives = $fact->search($search)->toArray();
        $ul = '';

        if (empty($additives))
            $ul = '<li><span>Нет результатов по вашему запросу</span></li>';

        foreach ($additives as $additive) {
            $value = $additive['code'] . ' ' . $additive['spec'];
            $value = $this->highlightStr($value, $search);
            $url = '/additive/show?id=' . $additive['id'];
            $ul .= '<li>
                        <a href="'. $url . '">
                            <span">' . $value .'</span>
                        </a>
                    </li>';
        }

        $response = new Response();
        $response->setContent(json_encode($ul));

        return $response;
    }

    /**
     * Подсветить совпадения
     * @param string $haystack
     * @param string $needle
     * @return mixed
     */
    private function highlightStr($haystack = '', $needle = ''):string
    {
        $start_pos = stripos(mb_strtolower($haystack), mb_strtolower($needle));
        $str = substr($haystack, $start_pos, strlen($needle));
        $value = str_ireplace($str, '<span class="highlight">' . $str . '</span>', $haystack);
        return $value;
    }

}

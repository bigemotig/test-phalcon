<?php namespace app\controllers;

use app\models\Article;

class IndexController extends BaseController
{
    public $isMain = true;
    public $controller_id = 1;

    public function initialize()
    {
        $this->breadcrumb = true;
        $this->view->setVar('articles', $this->lastArticles());
        parent::initialize();
    }

    public function indexAction()
    {
        $this->breadcrumbTitle = 'Справочник пищевых добавок';
        $this->view->setVar('breadcrumb_title', $this->breadcrumbTitle);
    }

    public function lastArticles()
    {
        $articles = Article::find([
            'limit' => Article::LAST_ARTICLES,
            'order' => 'id DESC'
        ]);
        $articles = $articles ? $articles->toArray() : [];

        foreach ($articles as &$article) {
            $short_announce = mb_substr($article['announcement'], 0, 340);
            $article['announcement'] = trim($short_announce) . '...';
        }

        return $articles;
    }

}


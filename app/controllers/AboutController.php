<?php namespace app\controllers;

class AboutController extends BaseController
{
    public function initialize()
    {
        $this->breadcrumb = true;
        $this->breadcrumbTitle = 'Информация о сайте';
        $this->view->setVar('site_email', $this->config->site->site_email);
        parent::initialize();
    }

    public function indexAction(){}

    public function breadcrumbs() {
        $breadcrumbs = [];
        return $breadcrumbs;
    }

}


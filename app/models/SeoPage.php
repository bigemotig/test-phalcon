<?php namespace app\models;

use Phalcon\Mvc\Model;

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 25.10.18
 * Time: 22:41
 */
class SeoPage extends Model
{
    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $controller_id;

    /**
     *
     * @var integer
     */
    protected $post_id;

    /**
     *
     * @var string
     */
    protected $title;

    /**
     *
     * @var string
     */
    protected $keywords;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var integer
     */
    protected $is_active;

    /**
     *
     * @var integer
     */
    protected $for_sitemap;

    /**
     *
     * @var integer
     */
    protected $updater_id;

    /**
     *
     * @var string
     */
    protected $update_time;

    /**
     *
     * @var integer
     */
    protected $creator_id;

    /**
     *
     * @var string
     */
    protected $create_time;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("edobavki");
        $this->setSource("sys_seo_page");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sys_seo_page';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SeoPage[]|SeoPage|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SeoPage|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getControllerId(): int
    {
        return $this->controller_id;
    }

    /**
     * @param int $controller_id
     */
    public function setControllerId(int $controller_id): void
    {
        $this->controller_id = $controller_id;
    }

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->post_id;
    }

    /**
     * @param int $post_id
     */
    public function setPostId(int $post_id): void
    {
        $this->post_id = $post_id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords(string $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getisActive(): int
    {
        return $this->is_active;
    }

    /**
     * @param int $is_active
     */
    public function setIsActive(int $is_active): void
    {
        $this->is_active = $is_active;
    }

    /**
     * @return int
     */
    public function getForSitemap(): int
    {
        return $this->for_sitemap;
    }

    /**
     * @param int $for_sitemap
     */
    public function setForSitemap(int $for_sitemap): void
    {
        $this->for_sitemap = $for_sitemap;
    }
}

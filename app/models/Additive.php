<?php namespace app\models;

use Phalcon\Mvc\Model;

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 25.10.18
 * Time: 22:41
 */
class Additive extends Model
{
    const ALLOWED               = 1;    // разрешена
    const DISALLOWED            = 0;    // запрещена
    const IN_RU_KEY             = 'in_russia';
    const IN_EU_KEY             = 'in_eu';

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $code;

    /**
     *
     * @var string
     */
    protected $spec;

    /**
     *
     * @var string
     */
    protected $functions;

    /**
     *
     * @var integer
     */
    protected $in_russia;

    /**
     *
     * @var string
     */
    protected $appearance;

    /**
     *
     * @var integer
     */
    protected $in_eu;

    /**
     *
     * @var string
     */
    protected $negative_effects;

    /**
     *
     * @var integer
     */
    protected $seq;

    /**
     *
     * @var integer
     */
    protected $order_number;

    /**
     *
     * @var string
     */
    protected $information;

    /**
     *
     * @var string
     */
    protected $employment;

    /**
     *
     * @var string
     */
    protected $extracted_from;

    /**
     *
     * @var integer
     */
    protected $updater_id;

    /**
     *
     * @var string
     */
    protected $update_time;

    /**
     *
     * @var integer
     */
    protected $creator_id;

    /**
     *
     * @var string
     */
    protected $create_time;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("edobavki");
        $this->setSource("adt_additive");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'adt_additive';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Additive[]|Additive|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Additive|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function allAdditives()
    {
        return $this->modelsManager->createBuilder()
            ->columns(['app\models\Additive.id', 'app\models\Additive.code', 'app\models\Additive.spec', 'app\models\Additive.seq', 'app\models\Additive.order_number',
                'app\models\SeoPage.title'])
            ->from(['app\models\Additive'])
            ->join('app\models\SeoPage', 'app\models\Additive.id = app\models\SeoPage.post_id')
            ->where("app\models\SeoPage.controller_id = '10'")
            ->orderBy('app\models\Additive.seq ASC, app\models\Additive.order_number ASC')
            ->getQuery()
            ->execute();
    }

    public function search(String $search  = '')
    {
        return $this->modelsManager->createBuilder()
            ->columns(['app\models\Additive.id', 'app\models\Additive.code', 'app\models\Additive.spec'])
            ->from(['app\models\Additive'])
            ->where('app\models\Additive.spec LIKE :search:', ['search' => '%' . $search . '%'])
            ->orWhere('app\models\Additive.code LIKE :search:', ['search' => '%' . $search . '%'])
            ->orderBy('app\models\Additive.seq ASC, app\models\Additive.order_number ASC')
            ->limit(10)
            ->getQuery()
            ->execute();
    }

    public function fullInfo(int $id):array
    {
        $additive = Additive::find([
            'id = ?0',
            'bind' => [$id]
        ])->getFirst()->toArray();

        $additives = $this->allAdditives()->toArray();

        for ($i = 0; $i < count($additives); $i++) {
            if ($additives[$i]['id'] == $id) {
                if ($i != 0) {
                    $additive['prev_id'] = $additives[$i - 1]['id'];
                    $additive['prev_code'] = $additives[$i - 1]['code'];
                    $additive['prev_title'] = $additives[$i - 1]['title'];
                }

                if ($i != count($additives) - 1) {
                    $additive['next_id'] = $additives[$i + 1]['id'];
                    $additive['next_code'] = $additives[$i + 1]['code'];
                    $additive['next_title'] = $additives[$i + 1]['title'];
                }
            }
        }

        foreach ($additive as $key => &$value)
            $value = $this->getValue($key, $value);

        return $additive;
    }

    public function getValue($key, $value)
    {
        $result = !empty($value) ? $value : null;
        switch ($key) {
            case Additive::IN_EU_KEY:
            case Additive::IN_RU_KEY:
                $result =$this->isEnabled($value);
                break;
            case 'spec':
            case 'appearance':
            case 'employment':
            case 'extracted_from':
                $result = preg_split('/\n/', $value);
                break;
        }

        return $result;
    }

    public function isEnabled(int $value = 0):string
    {
        return $value == Additive::ALLOWED ? 'Разрешена.' : 'Запрещена.';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getSpec(): string
    {
        return $this->spec;
    }

    /**
     * @param string $spec
     */
    public function setSpec(string $spec): void
    {
        $this->spec = $spec;
    }

    /**
     * @return string
     */
    public function getFunctions(): string
    {
        return $this->functions;
    }

    /**
     * @param string $functions
     */
    public function setFunctions(string $functions): void
    {
        $this->functions = $functions;
    }

    /**
     * @return int
     */
    public function getInRussia(): int
    {
        return $this->in_russia;
    }

    /**
     * @param int $in_russia
     */
    public function setInRussia(int $in_russia): void
    {
        $this->in_russia = $in_russia;
    }

    /**
     * @return string
     */
    public function getAppearance(): string
    {
        return $this->appearance;
    }

    /**
     * @param string $appearance
     */
    public function setAppearance(string $appearance): void
    {
        $this->appearance = $appearance;
    }

    /**
     * @return int
     */
    public function getInEu(): int
    {
        return $this->in_eu;
    }

    /**
     * @param int $in_eu
     */
    public function setInEu(int $in_eu): void
    {
        $this->in_eu = $in_eu;
    }

    /**
     * @return string
     */
    public function getNegativeEffects(): string
    {
        return $this->negative_effects;
    }

    /**
     * @param string $negative_effects
     */
    public function setNegativeEffects(string $negative_effects): void
    {
        $this->negative_effects = $negative_effects;
    }

    /**
     * @return int
     */
    public function getSeq(): int
    {
        return $this->seq;
    }

    /**
     * @param int $seq
     */
    public function setSeq(int $seq): void
    {
        $this->seq = $seq;
    }

    /**
     * @return int
     */
    public function getOrderNumber(): int
    {
        return $this->order_number;
    }

    /**
     * @param int $order_number
     */
    public function setOrderNumber(int $order_number): void
    {
        $this->order_number = $order_number;
    }

    /**
     * @return string
     */
    public function getInformation(): string
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation(string $information): void
    {
        $this->information = $information;
    }

    /**
     * @return string
     */
    public function getEmployment(): string
    {
        return $this->employment;
    }

    /**
     * @param string $employment
     */
    public function setEmployment(string $employment): void
    {
        $this->employment = $employment;
    }

    /**
     * @return string
     */
    public function getExtractedFrom(): string
    {
        return $this->extracted_from;
    }

    /**
     * @param string $extracted_from
     */
    public function setExtractedFrom(string $extracted_from): void
    {
        $this->extracted_from = $extracted_from;
    }


}

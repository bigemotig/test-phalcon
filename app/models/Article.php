<?php namespace app\models;

use Phalcon\Mvc\Model;

/**
 * Created by PhpStorm.
 * User: denis
 * Date: 25.10.18
 * Time: 22:41
 */
class Article extends Model
{
    const LAST_ARTICLES = 3; //количество последних статей

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $spec;

    /**
     *
     * @var string
     */
    protected $announcement;

    /**
     *
     * @var string
     */
    protected $content;

    /**
     *
     * @var string
     */
    protected $thumb_name;

    /**
     *
     * @var integer
     */
    protected $updater_id;

    /**
     *
     * @var string
     */
    protected $update_time;

    /**
     *
     * @var integer
     */
    protected $creator_id;

    /**
     *
     * @var string
     */
    protected $create_time;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("edobavki");
        $this->setSource("art_article");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'art_article';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Article[]|Article|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Article|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function getArticles() {
        $articles = Article::find()->toArray();

        foreach ($articles as &$article) {
            $short_annonce = mb_substr($article['announcement'], 0, 340);
            $article['announcement'] = trim($short_annonce) . '...';
        }

        return $articles;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSpec(): string
    {
        return $this->spec;
    }

    /**
     * @param string $spec
     */
    public function setSpec(string $spec): void
    {
        $this->spec = $spec;
    }

    /**
     * @return string
     */
    public function getAnnouncement(): string
    {
        return $this->announcement;
    }

    /**
     * @param string $announcement
     */
    public function setAnnouncement(string $announcement): void
    {
        $this->announcement = $announcement;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getThumbName(): string
    {
        return $this->thumb_name;
    }

    /**
     * @param string $thumb_name
     */
    public function setThumbName(string $thumb_name): void
    {
        $this->thumb_name = $thumb_name;
    }

}

<?php namespace app\models;

use Phalcon\Mvc\Model;

class Controller extends Model
{
    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $controller_name;

    /**
     *
     * @var string
     */
    protected $spec;

    /**
     *
     * @var string
     */
    protected $page_url;

    /**
     *
     * @var string
     */
    protected $action_url;

    /**
     *
     * @var integer
     */
    protected $module_id;

    /**
     *
     * @var integer
     */
    protected $no_index;

    /**
     *
     * @var string
     */
    protected $icon;

    /**
     *
     * @var double
     */
    protected $priority;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("edobavki");
        $this->setSource("sys_controller");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sys_controller';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Controller[]|Controller|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Controller|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getControllerName(): string
    {
        return $this->controller_name;
    }

    /**
     * @param string $controller_name
     */
    public function setControllerName(string $controller_name): void
    {
        $this->controller_name = $controller_name;
    }

    /**
     * @return string
     */
    public function getSpec(): string
    {
        return $this->spec;
    }

    /**
     * @param string $spec
     */
    public function setSpec(string $spec): void
    {
        $this->spec = $spec;
    }

    /**
     * @return string
     */
    public function getPageUrl(): string
    {
        return $this->page_url;
    }

    /**
     * @param string $page_url
     */
    public function setPageUrl(string $page_url): void
    {
        $this->page_url = $page_url;
    }

    /**
     * @return string
     */
    public function getActionUrl(): string
    {
        return $this->action_url;
    }

    /**
     * @param string $action_url
     */
    public function setActionUrl(string $action_url): void
    {
        $this->action_url = $action_url;
    }

    /**
     * @return int
     */
    public function getModuleId(): int
    {
        return $this->module_id;
    }

    /**
     * @param int $module_id
     */
    public function setModuleId(int $module_id): void
    {
        $this->module_id = $module_id;
    }

    /**
     * @return int
     */
    public function getNoIndex(): int
    {
        return $this->no_index;
    }

    /**
     * @param int $no_index
     */
    public function setNoIndex(int $no_index): void
    {
        $this->no_index = $no_index;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return float
     */
    public function getPriority(): float
    {
        return $this->priority;
    }

    /**
     * @param float $priority
     */
    public function setPriority(float $priority): void
    {
        $this->priority = $priority;
    }


}

<?php

$router = new Phalcon\Mvc\Router(false);
$router->setDefaultNamespace('app\controllers');

$router->add(
    '/',
    array(
        'controller' => 'index',
        'action' => 'index',
    )
)->setName('root');

$router->add(
    '/:controller/:action/:params',
    array(
        'controller' => 1,
        'action' => 2,
        'params' => 3
    )
)->setName('baseRoute');

$router->addGet(
    '/about_us',
    [
        'controller' => 'about',
        'action' => 'index'
    ]
);

$router->addGet(
    '/mobile_application',
    [
        'controller' => 'app',
        'action' => 'index'
    ]
);

$router->addGet(
    '/privacy_policy',
    [
        'controller' => 'privacy',
        'action' => 'index'
    ]
);

$router->notFound(
    [
        'controller' => 'error',
        'action'     => 'show404',
    ]
);


$router->removeExtraSlashes(true);

return $router;

<?php

$di->set('config', function() use ($config) {
    return $config;
});

$di->set('db', function() use ($config) {
    return new \Phalcon\Db\Adapter\Pdo\Mysql([
        "host" => $config->database->host,
        "port" => $config->database->port,
        "username" => $config->database->username,
        "password" => $config->database->password,
        "dbname" => $config->database->dbname
    ]);
});

$di->set('view', function() use ($config){
    $view = new Phalcon\Mvc\View();
    $view->registerEngines([
        '.volt' => function ($view) {
            $volt = new Phalcon\Mvc\View\Engine\Volt($view, $this);

            $volt->setOptions([
                'compiledPath' => SRC_PATH . '/cache/',
                'compiledSeparator' => '_'
            ]);

            return $volt;
        }
    ]);
    $view->setViewsDir(APP_PATH.'/views');
    return $view;
});

$di->set('router', function() use ($config) {
    return include APP_PATH.'/config/routes.php';
});

$di->set('assetsManager', function () {
    return new app\services\AssetsManager();
});

$di->set('dispatcher', function() use ($di) {
    $eventsManager = $di->getShared('eventsManager');
    $notFound = new app\plugins\NotFound404();
    $seo = new app\plugins\SeoPlugin();
    $eventsManager->attach('dispatch', $notFound);
    $eventsManager->attach('dispatch', $seo);
    $dispatcher = new Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});
